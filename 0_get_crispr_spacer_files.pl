#!/usr/bin/perl
use strict;
use autodie;
# Script to get the tetra frequency of viral populations
# Argument 0 : toto
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to extract the CRISPRs from CRASS, and get a spacer fasta file + a tab spacer to dr
# Argument 0 : toto\n";
	die "\n";
}

my @input=<rerun/*/*.crispr>;
my %spacer_to_dr;
my $i=0;

my $out_file_fasta="Crass_spacers.fna";
my $out_file_table="Crass_spacers_to_dr.tsv";
my $out_file_dr="Crass_direct-repeats.fna";

open my $s1,">",$out_file_fasta;
open my $s2,">",$out_file_table;
open my $s3,">",$out_file_dr;
print $s2 "## Spacer\tDirect repeat\tDR sequence\n";
my $j=1;
foreach my $file (@input){
	my $dr_c="";
	my $dr_seq="";
	open my $xml,"<",$file;
	while(<$xml>){
		chomp($_);
		if ($_=~/.*drid=\"(DR\d+)\" seq=\"([^\"]+)\"/){
			$dr_c=$1."_".$j;
			$dr_seq=$2;
			print $s3 ">$dr_c\n$dr_seq\n";
			$j++;
		}
		elsif($_=~/<dr /){
			print "Pblm with line $_ which should be a dr ??\n";
			<STDIN>;
		}
		elsif($_=~/<spacer.*seq=\"([^\"]*)\" spid=\"([^\"]*)\"/){
			my $seq=$1;
			my $id="Spacer_".$i."_".$2;
			$i++;
			print $s1 ">$id\n$seq\n";
			print $s2 "$id\t$dr_c\t$dr_seq\n";
		}
		elsif($_=~/<spacer /){
			print "Pblm with line $_ which should be a spacer ??\n";
			<STDIN>;
		}
	}
	close $xml;
}
close $s1;
close $s2;
close $s3;
