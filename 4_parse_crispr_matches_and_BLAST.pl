#!/usr/bin/perl
use strict;
use autodie;
# Script to get the tetra frequency of viral populations
# Argument 0 : toto
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to parse the BLAST and fuzznuc results
# Argument 0 : toto\n";
	die "\n";
}

my $th_length=20;
my $th_mismatch=1;
# my $th_evalue=1e-5;
# # my $th_coverage=80;

## Unchanged
my $fasta_spacer="Crass_spacers.fna";

# ## Round 1
# my $blast_result="Crass_spacers-vs-Abisko.tsv";
# my $fuzznuc_result_file="All_CRISPR_matches_with_motifs_detection.tsv";
# my $out_file="Crass_spacers_to_viral_contigs.tsv";

# ## Round 2
# my $blast_result="Crass_spacers-vs-Abisko_GBRdfS.tsv";
# my $fuzznuc_result_file="All_CRISPR_matches_with_motifs_detection_GBRdfS.tsv";
# my $out_file="Crass_spacers_to_viral_contigs_GBRdfS.tsv";

## Round 3
my $blast_result="Crass_spacers-vs-53_SoilVir.tsv";
my $fuzznuc_result_file="All_CRISPR_matches_with_motifs_detection_SoilVir.tsv";
my $out_file="Crass_spacers_to_viral_contigs_SoilVir.tsv";

my %store_seq;
my $id_c="";
open my $fa,"<",$fasta_spacer;
while(<$fa>){
	chomp($_);
	if ($_=~/^>(\S+)/){$id_c=$1;}
	else{$store_seq{$id_c}.=$_;}
}
close $fa;

my %store;
# my @list_fuzz=<$fuzznuc_result_dir/*.tsv>;
# foreach my $file (@list_fuzz){
# 	open my $tsv,"<",$file;
# 	while(<$tsv>){
# 		chomp($_);
# 		my @tab=split("\t",$_);
# 		if (defined($store{$tab[0]}{$tab[1]}{$tab[3]}{"fuzz"})){
# # 			print "Already a match of $tab[0] on $tab[1] starting at $tab[3] ?!\n";
# # 			<STDIN>;
# 			if ($tab[2]<$store{$tab[0]}{$tab[1]}{$tab[3]}{"fuzz"}){$store{$tab[0]}{$tab[1]}{$tab[3]}{"fuzz"}=$tab[2];}
# 		}
# 		else{
# 			$store{$tab[0]}{$tab[1]}{$tab[3]}{"fuzz"}=$tab[2];
# 		}
# 	}
# 	close $tsv;
# }

open my $tsv,"<",$fuzznuc_result_file;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if (defined($store{$tab[0]}{$tab[1]}{$tab[3]}{"fuzz"})){
# 			print "Already a match of $tab[0] on $tab[1] starting at $tab[3] ?!\n";
# 			<STDIN>;
		if ($tab[2]<$store{$tab[0]}{$tab[1]}{$tab[3]}{"fuzz"}){
			$store{$tab[0]}{$tab[1]}{$tab[3]}{"fuzz"}=$tab[2];
			$store{$tab[0]}{$tab[1]}{$tab[3]}{"fuzz_motif"}=$tab[6];
		}
	}
	else{
		$store{$tab[0]}{$tab[1]}{$tab[3]}{"fuzz"}=$tab[2];
		$store{$tab[0]}{$tab[1]}{$tab[3]}{"fuzz_motif"}=$tab[6];
	}
}
close $tsv;

open my $tsv,"<",$blast_result;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($tab[9]<$tab[8]){
		print "$tab[0] - $tab[1] => $tab[8] becomes $tab[9] because it's $tab[8] = $tab[9]\n";
		$tab[8]=$tab[9];
		if ($tab[7]<length($store_seq{$tab[0]})){
			$tab[8]-=length($store_seq{$tab[0]})-$tab[7];
		}
# 		<STDIN>;
	}
	elsif ($tab[6]>1){
		print "$tab[0] - $tab[1] => $tab[6] is the start, so $tab[8] becomes ";
		$tab[8]-=($tab[6]-1);
		print "$tab[8]\n";
# 		<STDIN>;
	}
	if (defined($store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"})){
# 		print "Already a match of $tab[0] on $tab[1] starting at $tab[8] with BLAST ?!\n";
# 		<STDIN>;
	}
	else{
		if ($tab[3]>length($store_seq{$tab[0]}) || $tab[5]>0){
			print "There is/are $tab[5] gaps opened, we don't consider\n";
# 			<STDIN>;
			if ($tab[5]==0){
				print "$_ is very very weird ???!\n";
				<STDIN>;
			}
		}
		else{
			$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"e-value"}=$tab[10];
			$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"score"}=$tab[11];
			$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"match_length"}=$tab[3];
			my $n_mismatch=length($store_seq{$tab[0]})-($tab[3]*$tab[2]/100);
			print "$tab[0] vs $tab[1] ali on $tab[3] with $tab[2] id and the spacer is ".length($store_seq{$tab[0]})." so we calculate that this is $n_mismatch mismatches\n";
			$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"id"}=sprintf("%0.2f",($tab[3]*$tab[2]/100)/length($store_seq{$tab[0]}));
			if ($tab[3]>length($store_seq{$tab[0]})){$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"id"}=$tab[2];} # Cases with gaps opened
	# 		<STDIN>;
			$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"n_mismatch"}=sprintf("%.0f",$n_mismatch);
	# 		my $cover=($tab[3])/length($store_seq{$tab[0]})*100;
	# 		$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"coverage"}=sprintf("%.02f",$cover);
		}
	}
}
close $tsv;

my $tag=0;
my $tag_1=0;
open my $s1,">",$out_file;
my %vu;
my %count;
print $s1 "Spacer,Hit,Start,Mismatches_fuzznuc,Motif_fuzznuc,Blast_evalue,BLAST_score,BLAST_hsp_length,BLAST_id_pcent_hsp,BLAST_mismatches,Spacer_sequence\n";
foreach my $spacer (sort keys %store){
	if (!defined($store_seq{$spacer})){
		print "No sequence for $spacer ??\n";
		<STDIN>;
	}
	if (length($store_seq{$spacer})>=$th_length){
		$tag=0;
		foreach my $hit (sort keys %{$store{$spacer}}){
			$tag_1=0;
			foreach my $start (sort keys %{$store{$spacer}{$hit}}){
				# We take hits with less than 2 mismatches and/or e-value 10-10 on > 80% of the spacer
				if ((defined($store{$spacer}{$hit}{$start}{"fuzz"}) && $store{$spacer}{$hit}{$start}{"fuzz"}<=$th_mismatch) || (defined($store{$spacer}{$hit}{$start}{"blast"}) && $store{$spacer}{$hit}{$start}{"blast"}{"n_mismatch"}<=$th_mismatch)){
					my $line=$spacer.",".$hit.",".$start;
					if (defined($store{$spacer}{$hit}{$start}{"fuzz"})){
						$line.=",".$store{$spacer}{$hit}{$start}{"fuzz"}.",".$store{$spacer}{$hit}{$start}{"fuzz_motif"};
					}
					else {
						print "!!!### Weird, no fuzznuc ????\n";
						print "$line\n";
						<STDIN>;
						$line.=",-,-";
					}
					if (defined($store{$spacer}{$hit}{$start}{"blast"})){
						$line.=",".$store{$spacer}{$hit}{$start}{"blast"}{"e-value"}.",".$store{$spacer}{$hit}{$start}{"blast"}{"score"}.",".$store{$spacer}{$hit}{$start}{"blast"}{"match_length"}.",".$store{$spacer}{$hit}{$start}{"blast"}{"id"}.",".$store{$spacer}{$hit}{$start}{"blast"}{"n_mismatch"};
						$line.=",".$store_seq{$spacer};
						print $s1 "$line\n";
						$tag=1;
						$tag_1=1;
						if (!defined($vu{$spacer}{$hit})){
							$vu{$spacer}{$hit}=1;
							$count{$spacer}++;
						}
					}
					else {
						# We don't take this one, no BLAST hit, means low complexity
# 						$tag_2=1;
# 						$line.=",-,-,-,-,-";
					}
					
				}
			}
			if ($tag_1==1){
				print $s1 "\n";
			}
		}
		if ($tag==1){
			print $s1 "####\n";
		}
	}
}
close $s1;

my %super_count;
foreach my $spacer (keys %count){
	$super_count{$count{$spacer}}++;
	if ($count{$spacer}>5){
		print "$spacer => $count{$spacer}\n";
	}
}

print "#####\n";
foreach (sort { $a <=> $b } keys %super_count){
	print "$_\t$super_count{$_}\n";
}
print "\n";