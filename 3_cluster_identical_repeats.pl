#!/usr/bin/perl
use strict;
use autodie;
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to cluster identical repeats together
# Argument 0 : toto\n";
	die "\n"
}

my $fa_file_dr="Crass_direct-repeats.fna";
my $clstr_file="Crass_direct-repeats-nr_v2.clstr";

my %seen;
my %clust;

my $c_c="";
my $n_c=0;
open my $fa,"<",$fa_file_dr;
while(<$fa>){
	chomp($_);
	if ($_=~/^>(.*)/){
		$c_c=$1;
	}
	else{
		if ($seen{$_} ne ""){
			$clust{$seen{$_}}{$c_c}=1;
		}
		else{
			$n_c++;
			$seen{$_}="Cluster_".$n_c;
			$clust{$seen{$_}}{$c_c}=1;
		}
	}
}
close $fa;

open my $s1,">",$clstr_file;
foreach my $cl (sort keys %clust){
	print $s1 "$cl\t".join(",",sort keys %{$clust{$cl}})."\n";
}
close $s1;