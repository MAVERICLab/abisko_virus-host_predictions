#!/usr/bin/perl
use strict;
use autodie;
# Script to get the tetra frequency of viral populations
# Argument 0 : toto
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to parse the BLAST of dr
# Argument 0 : toto\n";
	die "\n";
}

my $th_evalue=1e-10;
my $th_coverage=100;
my $th_id=100;

my $blast_result="Crass_dr-vs-Microbial-bins.tsv";
my $out_file="Crass_dr_to_microbial_bins-bis.tsv";
my $fasta_dr="Crass_direct-repeats.fna";

my %store_seq;
my $id_c="";
open my $fa,"<",$fasta_dr;
while(<$fa>){
	chomp($_);
	if ($_=~/^>(\S+)/){$id_c=$1;}
	else{$store_seq{$id_c}.=$_;}
}
close $fa;

my %store;
open my $tsv,"<",$blast_result;
while(<$tsv>){
	chomp($_);
	my @tab=split("\t",$_);
	if ($tab[9]<$tab[8]){$tab[8]=$tab[9];}
	if (defined($store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"})){
# 		print "Already a match of $tab[0] on $tab[1] starting at $tab[8] with BLAST ?!\n";
# 		<STDIN>;
	}
	else{
		$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"e-value"}=$tab[10];
		$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"score"}=$tab[11];
		$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"match_length"}=$tab[3];
		$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"id"}=sprintf("%.02f",$tab[3]*$tab[2]/length($store_seq{$tab[0]}));
		if ($tab[3]>length($store_seq{$tab[0]})){$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"id"}=$tab[2];} # Cases with gaps opened
		my $cover=($tab[3])/length($store_seq{$tab[0]})*100;
		$store{$tab[0]}{$tab[1]}{$tab[8]}{"blast"}{"coverage"}=sprintf("%.02f",$cover);
	}
}
close $tsv;

my $tag=0;
my $tag_1=0;
open my $s1,">",$out_file;
my %vu;
my %count;
print $s1 "Direct_repeat,Hit,Start,Blast_evalue,BLAST_score,BLAST_hsp_length,BLAST_id_pcent,BLAST_id_coverage,Direct_repeat_sequence\n";
foreach my $dr (sort keys %store){
	if (!defined($store_seq{$dr})){
		print "No sequence for $dr ??\n";
		<STDIN>;
	}
	foreach my $hit (sort keys %{$store{$dr}}){
		$tag_1=0;
		foreach my $start (sort keys %{$store{$dr}{$hit}}){
			if ($store{$dr}{$hit}{$start}{"blast"}{"id"}>=$th_id && $store{$dr}{$hit}{$start}{"blast"}{"coverage"}>=$th_coverage){
				my $line=$dr.",".$hit.",".$start.",".$store{$dr}{$hit}{$start}{"blast"}{"e-value"}.",".$store{$dr}{$hit}{$start}{"blast"}{"score"}.",".$store{$dr}{$hit}{$start}{"blast"}{"match_length"}.",".$store{$dr}{$hit}{$start}{"blast"}{"id"}.",".$store{$dr}{$hit}{$start}{"blast"}{"coverage"};
				$line.=",".$store_seq{$dr};
				print $s1 "$line\n";
				$tag=1;
				$tag_1=1;
				if (!defined($vu{$dr}{$hit})){
					$vu{$dr}{$hit}=1;
					$count{$dr}++;
				}
			}
		}
		if ($tag_1==1){
			print $s1 "####\n";
		}
	}
}
close $s1;

my %super_count;
foreach my $dr (keys %count){
	$super_count{$count{$dr}}++;
	if ($count{$dr}>5){
		print "$dr => $count{$dr}\n";
	}
}

print "#####\n";
foreach (sort { $a <=> $b } keys %super_count){
	print "$_\t$super_count{$_}\n";
}
print "\n";