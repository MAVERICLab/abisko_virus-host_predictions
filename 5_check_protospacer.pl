#!/usr/bin/perl
use strict;
use autodie;
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to get the sequence just in 5' of the CRISPR matches
# Argument 0 : toto\n";
	die "\n"
}
### First round
# my $fasta_in="Abisko_virus_PSEonly_VirSorted_contigs_over10kb_or_circular_cats1245_for_vContact.fasta";
# my $fuzznuc_result_dir="CRISPR_matches/";
# my $out_file="All_CRISPR_matches_with_motifs_detection.tsv";
### Second round
# my $fasta_in="Abisko_viruses_Garys_BenBs_RefSeq_detected_for_Simon_virusHost.fasta";
# my $fuzznuc_result_dir="CRISPR_matches_2/";
# my $out_file="All_CRISPR_matches_with_motifs_detection_GBRdfS.tsv";
### Third round
my $fasta_in="53_SoilVir_contigs.fasta";
my $fuzznuc_result_dir="CRISPR_matches_3/";
my $out_file="All_CRISPR_matches_with_motifs_detection_SoilVir.tsv";

my %all_seq;
my $id_c="";
open my $fa,"<",$fasta_in;
while(<$fa>){
	chomp($_);
	if ($_=~/^>(.*)/){
		$id_c=$1;
	}
	else{
		$all_seq{$id_c}.=$_;
	}
}
close $fa;


my %store;
my @list_fuzz=<$fuzznuc_result_dir/*.tsv>;
open my $s1,">",$out_file;
my %count;
foreach my $file (@list_fuzz){
	open my $tsv,"<",$file;
	while(<$tsv>){
		chomp($_);
		my @tab=split("\t",$_);
		my $five_prim=substr($all_seq{$tab[1]},$tab[3]-6,5);
		my $three_prim=substr($all_seq{$tab[1]},$tab[4],5);
		my $spacer=substr($all_seq{$tab[1]},$tab[3]-1,length($tab[5]));
		my $r_spacer=&revcomp($spacer);
		my $whole_region=substr($all_seq{$tab[1]},$tab[3]-6,length($tab[5])+10);
		# First set of motifs : NGG or GG (or NCC / CC)
		my $output="";
		if ($five_prim=~/CC[ATCG]{0,1}$/){
			$output="Look, in $tab[1], in 5', motif 1 NGG: $five_prim - $tab[5]\n$five_prim - $tab[5] - $three_prim \n $spacer \n $r_spacer \n $whole_region \n";
			$count{$tab[2]}{"motif1_GGN"}++;
			print $s1 "$_\tGGN\n";
# 			<STDIN>;
		}
		elsif($three_prim=~/^[ATCG]{0,1}GG/){
			$output="Look, in $tab[1], in 3', motif 1 NGG: $three_prim - $tab[5]\n$five_prim - $tab[5] - $three_prim\n$spacer \n$r_spacer \n$whole_region\n";
			$count{$tab[2]}{"motif1_GGN"}++;
			print $s1 "$_\tGGN\n";
# 			<STDIN>;
		}
		# SEcond motif CNT
		elsif($five_prim=~/A[ATCG]G$/){
			$output="Look, in $tab[1], in 5', motif 2 CNT: $five_prim - $tab[5]\n$five_prim - $tab[5] - $three_prim\n$spacer \n$r_spacer \n$whole_region\n";
			$count{$tab[2]}{"motif2_CNT"}++;
			print $s1 "$_\tCNT\n";
# 			<STDIN>;
		}
		elsif($three_prim=~/^C[ATCG]T/){
			$output="Look, in $tab[1], in 3', motif 2 CNT: $three_prim - $tab[5]\n$five_prim - $tab[5] - $three_prim\n$spacer \n$r_spacer \n$whole_region\n";
			$count{$tab[2]}{"motif2_CNT"}++;
			print $s1 "$_\tCNT\n";
# 			<STDIN>;
		}
		# Third motif GAA
		elsif($five_prim=~/TTC$/){
			$output="Look, in $tab[1], in 5', motif 3 GAA: $five_prim - $tab[5]\n$five_prim - $tab[5] - $three_prim\n$spacer \n$r_spacer \n$whole_region\n";
			$count{$tab[2]}{"motif3_GAA"}++;
			print $s1 "$_\tGAA\n";
# 			<STDIN>;
		}
		elsif($three_prim=~/^GAA/){
			$output="Look, in $tab[1], in 3', motif 3 GAA: $three_prim - $tab[5]\n$five_prim - $tab[5] - $three_prim\n$spacer \n$r_spacer \n$whole_region\n";
			$count{$tab[2]}{"motif3_GAA"}++;
			print $s1 "$_\tGAA\n";
# 			<STDIN>;
		}
		else{
			# no motif detected
			$output="In $tab[1], no motif \n$five_prim - $tab[5] - $three_prim\n$spacer \n$r_spacer \n$whole_region\n";
			$count{$tab[2]}{"no_motif"}++;
			print $s1 "$_\t-\n";
		}
		print $output;
# 		<STDIN>;
	}
	close $tsv;
}
close $s1;
print "Mismatches\tMotif_GG[N]\tMotif_CNT\tMotif_GAA\tNo_Motif\n";
foreach my $score (sort keys %count){
	print "$score\t$count{$score}{motif1_GGN}\t$count{$score}{motif2_CNT}\t$count{$score}{motif3_GAA}\t$count{$score}{no_motif}\n";
}


sub revcomp{
	my $seq=$_[0];
	$seq=~tr/atcg/tagc/;
	$seq=~tr/ATCG/TAGC/;
	$seq=reverse($seq);
	return $seq;
}