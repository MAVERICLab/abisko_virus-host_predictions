#!/usr/bin/perl
use strict;
use autodie;
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to get the Cas genes and Minced predictions of CRISPR arrays and add these on DR matches
# Argument 0 : toto\n";
	die "\n"
}

my $gff_proka="prokka2_locustagged.gff.gz";
my $dr_file="Crass_dr_to_microbial_bins.tsv";
my $out_file="Crass_dr_to_microbial_bins-with_Cas_minced.csv";


# Read the gff file 
my %vu;
my %check_cas;
my %check_minced;
my %seen;
open my $gff,"gunzip -c $gff_proka |";
while (<$gff>){
	chomp($_);
	if ($_=~/^##/){next;}
	if ($_=~/^\.\/\.\//){next;}
	if ($_ eq ""){next;}
	my @tab=split(" ",$_);
	if ($tab[2] eq "CDS"){
		my $strain;
		$seen{$tab[0]}=1;
		for (my $i=8;$i<=$#tab;$i++){$strain.=$tab[$i]." ";}
		if ($strain=~/.*product=(.*)/){
			my $prod=$1;
# 			print "$tab[0] - $prod - $tab[3] - $tab[4]\n";
			if ($prod=~/.*Cas.*/ || $prod=~/.*CRISPR.*/){
				if ($prod=~/Caspase/ && !($prod=~/.*CRISPR.*/)){next;}
				if ($prod=~/Castor/ && !($prod=~/.*CRISPR.*/)){next;}
				if ($prod=~/Beta-Casp/ && !($prod=~/.*CRISPR.*/)){next;}
				$check_cas{$tab[0]}{$tab[3]}=1;
				$check_cas{$tab[0]}{$tab[4]}=1;
				if (!defined($vu{$prod})){
					print "We like $prod ! \n";
# 					<STDIN>;
					$vu{$prod}=1;
				}
			}
		}
	}
	elsif($tab[1]=~/^minced/){
		print "We found a minced prediction on $tab[0] from $tab[3] to $tab[4]\n";
		$check_minced{$tab[0]}{$tab[3]}=1;
		$check_minced{$tab[0]}{$tab[4]}=1;
	}
}
close $gff;

# Now read the DR and check if there is a CRISPR-associated protein or minced prediction within 1kb
my $shift=1000;
open my $csv,"<",$dr_file;
open my $s1,">",$out_file;
print $s1 "Direct_repeat,Hit,Start,Blast_evalue,BLAST_score,BLAST_hsp_length,BLAST_id_pcent,BLAST_id_coverage,Direct_repeat_sequence,Cas,Minced\n";
while(<$csv>){
	chomp($_);
	if (($_=~/^Direct_repeat,Hit/) || ($_=~/^####/)){next;}
	my @tab=split(",",$_);
	if ($seen{$tab[1]}!=1){
		print "?! We never saw $tab[1] in the prokka file ??\n";
		<STDIN>;
	}
	my $tag_cas=0;
	my $tag_minced=0;
	for (my $i=($tab[2]-$shift);$i<=($tab[2]+$tab[5]+$shift);$i++){
		if ($check_cas{$tab[1]}{$i}==1){$tag_cas=1;}
		if ($check_minced{$tab[1]}{$i}==1){$tag_minced=1;}
		if ($tag_cas == 1 && $tag_minced == 1){$i=$tab[2]+$tab[5]+$shift+1;}
	}
	print "$tab[1] - $tag_cas / $tag_minced\n";
	print $s1 "$_,$tag_cas,$tag_minced\n";
}
close $csv;
close $s1;